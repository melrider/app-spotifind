# Spotifind

Application web permettant de rechercher des artistes sur Spotify, puis d’afficher la couverture et le nom des albums d’un artiste, ainsi que les chansons associées classées par popularité.

Enfin, l’application proposera un historique en page d’accueil, permettant :
d’accéder aux 5 derniers artistes consultés,
la liste sera affichée même après rechargement de la page

## Table of Contents

1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)

## General Info

Front-end

## Technologies

A list of technologies used within the project:

- [Docker](https://docs.docker.com/): Version 3.9
- [VueJs](https://vuejs.org/): Version 3

## Installation

Pour le dev port 80

Créer un fichier .env à la racine de /client avec deux variables :
VUE_APP_CLIENT_ID
VUE_APP_CLIENT_SECRET

```
$ git clone https://gitlab.com/melrider/app_gibsindustry
$ cd ../path/to/the/file
$ docker-compose up --build -d
```
