import { Buffer } from "buffer";

const clientId = process.env.VUE_APP_CLIENT_ID;
const clientSecret = process.env.VUE_APP_CLIENT_SECRET;

async function getAccessToken() {
  const authOptions = {
    url: "https://accounts.spotify.com/api/token",
    headers: {
      Authorization:
        "Basic " +
        new Buffer.from(clientId + ":" + clientSecret).toString("base64"),
      "Content-Type": "application/json",
    },
    form: {
      grant_type: "client_credentials",
    },
    json: true,
  };
  try {
    const response = await fetch(authOptions.url, {
      method: "POST",
      body:
        "grant_type=client_credentials&client_id=" +
        clientId +
        "&client_secret=" +
        clientSecret,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
    const access_token = await response.json();
    return access_token.access_token;
  } catch (e) {
    console.error(`Error token : ${e}`);
  }
}

export async function getArtist(value) {
  const token = await getAccessToken();
  try {
    if (value.name && value.name.length) {
      const resultInfo = await fetch(
        `https://api.spotify.com/v1/search?query=${value.name}&&type=artist`,
        {
          method: "GET",
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      return await resultInfo.json();
    }
  } catch (e) {
    console.error(`Error token : ${e}`);
  }
}

export async function getArtistAlbum(key) {
  const token = await getAccessToken();
  try {
    const result = await fetch(
      `https://api.spotify.com/v1/artists/${key}/albums`,
      {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      }
    );
    return await result.json();
  } catch (e) {
    console.error(`Error token : ${e}`);
  }
}

export async function getAlbumInfo(albumId) {
  const token = await getAccessToken();
  try {
    const result = await fetch(`https://api.spotify.com/v1/albums/${albumId}`, {
      method: "GET",
      headers: { Authorization: `Bearer ${token}` },
    });
    return await result.json();
  } catch (e) {
    console.error(`Error token : ${e}`);
  }
}

export async function getTrackInfo(trackId) {
  const token = await getAccessToken();
  try {
    const result = await fetch(`https://api.spotify.com/v1/tracks/${trackId}`, {
      method: "GET",
      headers: { Authorization: `Bearer ${token}` },
    });
    return await result.json();
  } catch (e) {
    console.error(`Error token : ${e}`);
  }
}
