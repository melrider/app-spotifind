import { createRouter, createWebHistory } from "vue-router";
import HomePage from "./components/HomePage.vue";
import SearchArtist from "./components/SearchArtist.vue";

const routes = [
  {
    path: "/",
    component: () => import("./components/HomePage.vue"),
    name: "HomePage",
  },
  {
    path: "/search/:key",
    component: () => import("./components/SearchArtist.vue"),
    name: "SearchArtist",
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
